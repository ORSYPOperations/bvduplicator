package com.orsyp.tools.bvanalyser.api;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.SerializationUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.orsyp.Area;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.SyntaxException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.Product;
import com.orsyp.api.Timezone;
import com.orsyp.api.bv.BusinessView;
import com.orsyp.api.bv.BusinessViewId;
import com.orsyp.api.bv.BvmFilter;
import com.orsyp.api.bv.BvmItem;
import com.orsyp.api.bv.BvmList;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.mu.Mu;
import com.orsyp.api.mu.MuFilter;
import com.orsyp.api.mu.MuList;
import com.orsyp.api.resource.Resource;
import com.orsyp.api.resource.ResourceId;
import com.orsyp.api.security.Operation;
import com.orsyp.api.session.Session;
import com.orsyp.api.session.SessionId;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskFilter;
import com.orsyp.api.task.TaskId;
import com.orsyp.api.task.TaskList;
import com.orsyp.api.user.User;
import com.orsyp.api.user.UserFilter;
import com.orsyp.api.user.UserList;
import com.orsyp.api.user.UserSystem;
import com.orsyp.api.user.UserType;
import com.orsyp.central.jpa.jpo.NodeInfoEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.owls.impl.bv.OwlsBusinessViewImpl;
import com.orsyp.owls.impl.bv.OwlsBvmListImpl;
import com.orsyp.owls.impl.mu.OwlsMuImpl;
import com.orsyp.owls.impl.mu.OwlsMuListImpl;
import com.orsyp.owls.impl.resource.OwlsResourceImpl;
import com.orsyp.owls.impl.session.OwlsSessionImpl;
import com.orsyp.owls.impl.task.OwlsTaskImpl;
import com.orsyp.owls.impl.task.OwlsTaskListImpl;
import com.orsyp.owls.impl.user.OwlsUserImpl;
import com.orsyp.owls.impl.user.OwlsUserListImpl;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.MultiCentralConnectionFactory;
import com.orsyp.std.central.UniCentralStdImpl;
import com.orsyp.util.io.Lines;

public class DuApiConnection {
	
	private Context context;
	
	public DuApiConnection(String node, String area, String host, int port, String user, String password) {
		try {
			getNodeConnection(node, area, host, port, user, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void getNodeConnection(String node, String area, String host, int port, String user, String password) throws Exception{
		
		UniCentral central = getUVMSConnection(host, port, user, password);			
		NodeInfoEntity[] nnes = ClientServiceLocator.getNodeInfoService().getAllNodeInfoFromCache(-1, null);
		String company = null;
		
		for (NodeInfoEntity nne : nnes)
			if (nne.getProductCode().equals("DUN"))
				if (nne.getNodeName().equalsIgnoreCase(node)) {
					company = nne.getCompany();
					break;
				}
		
		if (company==null)
			throw new Exception("Node not found");
		
		/*
		if (Arrays.asList("A", "APP", "I", "INT").contains(area.toUpperCase())) 
			defaultVersion = "001";
		else
			defaultVersion = "000";
		*/
		
		Area a = Area.Exploitation;
		if (Arrays.asList("A", "APP").contains(area.toUpperCase()))
			a = Area.Application;
		else
		if (Arrays.asList("I", "INT").contains(area.toUpperCase()))
			a = Area.Integration;
		else
		if (Arrays.asList("S", "SIM").contains(area.toUpperCase()))
			a = Area.Simulation;
		
		context = makeContext(node, company, central, user, a);
	}
	
	private UniCentral getUVMSConnection(String host, int port, String user, String password) throws SyntaxException {		
		UniCentral cent = new UniCentral(host, port);
		cent.setImplementation(new UniCentralStdImpl(cent));
		
		Context ctx = new Context(new Environment("UJCENT",host), new Client(new Identity(user, password, host, "")));
		ctx.setProduct(com.orsyp.api.Product.UNICENTRAL);
		ctx.setUnijobCentral(cent);
		ClientServiceLocator.setContext(ctx);

		try {
			cent.login(user, password);
			if (ClientConnectionManager.getDefaultFactory() == null) {
	            ClientConnectionManager.setDefaultFactory(MultiCentralConnectionFactory.getInstance());
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return cent;
	}
	
	private Context makeContext(String node, String company, UniCentral central, String user, Area area) throws SyntaxException {
		Context ctx = null;
		Client client = new Client(new Identity(user, "", node, ""));
		ctx = new Context(new Environment(company, node, area), client, central);
		ctx.setProduct(Product.OWLS);
		return ctx;
	}

	
	//-------------------------------------------
	
	
	
	public List<String> getMus() throws Exception {
		ArrayList<String> list = new ArrayList<String>();
		MuList l = new MuList(context, new MuFilter());
    	l.setImpl(new OwlsMuListImpl());
		l.extract();
		
		for (int i = 0; i < l.getCount(); i++)
			list.add(l.get(i).getName());		
		return list;
	}
	
	public List<String> getUsers() throws Exception {
		ArrayList<String> list = new ArrayList<String>();
		UserList l = new UserList(context, new UserFilter());
    	l.setImpl(new OwlsUserListImpl());
		l.extract();
		
		for (int i = 0; i < l.getCount(); i++)
			list.add(l.get(i).getName());		
		return list;
	}
	
	public Context getContext() {
		return context;
	}
	
	//-----------------------------------------
	
	
	public List<String> getBVList() {
		ArrayList<String> list = new ArrayList<String>();
	    
        try {
    		BvmList l = new BvmList(context, new BvmFilter());
    	    l.setImpl(new OwlsBvmListImpl());	  
			l.extract(Operation.DISPLAY);
			for (int i = 0; i < l.getCount(); i++) {
				BvmItem item = l.get(i);				
				if (item.getName().toUpperCase().endsWith(".BV")) 				
					list.add(item.getName());
			}		
        } catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public BusinessView getBV(String bvName) {
		try {
        	
        	BusinessViewId id = new BusinessViewId(bvName);
        	id.setSyntaxRules(OwlsSyntaxRules.getInstance ());
        	
        	BusinessView obj = new BusinessView(context, id);

            obj.setImpl( new OwlsBusinessViewImpl() );
            obj.extract();
            
            return obj;
        	
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}
	
	public List<Task> getBVTasks(BusinessView bv) {
		ArrayList<Task> tasks = new ArrayList<Task>();
		
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	        
	        String xml = "";
			for (String l : bv.getData().getLines()) 
				xml+=l.replace(">", ">\n");						
			xml = xml.replace("hiveInternals:__hiveAttributesTypes__", "h");
	        
	        Document doc = docBuilder.parse(new InputSource(new StringReader(xml)));
	
	        doc.getDocumentElement().normalize();
	        
	        NodeList tskList = doc.getElementsByTagName("TaskDesign");
	        
	        for(int i=0; i<tskList.getLength() ; i++){
	            Node tsk = tskList.item(i);
	            if(tsk.getNodeType() == Node.ELEMENT_NODE) {
	            	String tskName = ((Element)tsk).getAttribute("taskName");
	            	String mu =  ((Element)tsk).getAttribute("templateName");
	            	try {
		            	tasks.add(getTask(tskName,mu));
	            	} catch (Exception e) {
	            		System.out.println("Error reading task: "+ tskName);
	            		e.printStackTrace();
	            	}
	            }
	        }
		}
		catch (Exception e) {
			e.printStackTrace();
		} 
        return tasks;
	}
	

	public Resource getResource(String name) throws Exception {
		Resource r = new Resource(context, ResourceId.create(name));
		r.setImpl(new OwlsResourceImpl());
		r.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		r.extract();
		return r;
	}

	public Session getSession(String name) throws Exception {
		SessionId sessionId = new SessionId(name);
		Session s = new Session(context, sessionId);
		s.setImpl(new OwlsSessionImpl());
		s.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		s.extract();
		return s;
	}

	public Task getTask(String name, String mu) throws Exception {
		String muFilter = "";
		if (mu!=null)
			muFilter=mu;
		TaskFilter filter = new TaskFilter(name, muFilter, "*", "*", "*", "*", "*");
        filter.setTemplate(true);
        filter.setType(null);        
        filter.setParentTaskMu("*");
        filter.setParentTask("*");
        
        TaskList list = new TaskList(context, filter);
        list.setImpl(new OwlsTaskListImpl());
        list.extract();
        
        Task t = null;
        
        if (list.getCount()==0)
        	throw new Exception("No task found: " + name);
                
        if (list.getCount()>1)
        	throw new Exception("More than 1 task found: " + name);
        	
        for (int idx=0; idx<list.getCount(); idx++) {
    		t = new Task(context, list.get(idx).getIdentifier());
    		t.setImpl(new OwlsTaskImpl());
    		t.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
    		t.extract();    	
        }        
        return t;
	}

	public void copyTask(Task task, String newTaskName, String taskUser, String taskMU)  throws Exception{
		Task t = SerializationUtils.clone(task);
		t.setContext(getContext());
		t.setImpl(new OwlsTaskImpl());		
		TaskId tId = TaskId.createWithName(task.getIdentifier().getName(), task.getIdentifier().getVersion(), task.getIdentifier().getMuName(), task.getIdentifier().isTemplate());
        tId.setSyntaxRules(OwlsSyntaxRules.getInstance());
        tId.setSessionName(task.getSessionName());
        tId.setSessionVersion(task.getSessionVersion());
        tId.setUprocName(task.getUprocName());
        tId.setUprocVersion(task.getUprocVersion());
		
		if (newTaskName!=null)
			t.getIdentifier().setName(newTaskName);
		if (taskUser!=null)
			t.setUserName(taskUser);
		if (taskMU!=null)
			t.setMuName(taskMU);
		t.create();
	}

	public void copyBV(BusinessView businessView, String newName, Lines newXML) throws Exception{		
    	BusinessView bv = SerializationUtils.clone(businessView);
    	bv.setImpl(new OwlsBusinessViewImpl());
    	BusinessViewId id = new BusinessViewId(newName);
    	id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		bv.setIdentifier(id);
		bv.setContext(getContext());		
		if (newXML!=null)
			bv.setData(newXML);
		bv.create();
	}

	public void createUser(String name) throws Exception {
		User obj = new User(getContext(), name);
		obj.setAuthorCode("");
		obj.setProfile("profadm");
		obj.setUserType(UserType.Both);
		obj.setLabel("Automatically created by BV Duplicator");

		UserSystem userSystem = new UserSystem();
		userSystem.setUser(name);
		userSystem.setPsw("");
		userSystem.setType(UserSystem.Type.OTHER);
		obj.setUserSystem(userSystem);

		obj.setImpl(new OwlsUserImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.create();
		
	}

	public void createMu(String name) throws Exception {
		Context ctx = getContext();
		Mu obj = new Mu(ctx, name);
		obj.setImpl(new OwlsMuImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.setLabel("Automatically created by BV Duplicator");
		obj.setNodeName(ctx.getEnvironment().getNodeName());
		obj.setDevelopmentAuth(true);
		obj.setProductionAuth(true);
		obj.setTimezone(new Timezone(Timezone.Sign.PLUS, 0, 0));
		obj.create();
	}	
}
