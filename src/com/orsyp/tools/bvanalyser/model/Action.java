package com.orsyp.tools.bvanalyser.model;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import com.orsyp.api.task.Task;

public class Action implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum Type {RENAME_BV, RENAME_TASK, CHANGE_USER, CHANGE_MU};
	
	public Type type;
	public Action parentAction;	
	public FilterData filter;	
	public RenameData rename;

	public Action(Type type, FilterData filter, RenameData rename, Action parentAction) {
		super();
		this.type = type;
		this.filter = filter;
		this.rename = rename;
		this.parentAction = parentAction;
	}
	
	
	public String getName() {
		switch (type) {
			case CHANGE_MU:		return "Change MU";
			case CHANGE_USER:	return "Change User";
			case RENAME_BV:		return "Rename BV";
			case RENAME_TASK:	return "Rename Task";
		}
		return "Unknown";
	}
	
	public String getFilterString() {
		return filter.getDescription();
	}
	
	public String getRenameString() {
		return rename.getDescription();
	}
	
	public Vector<String> getFilterResults(BVProject project, boolean highlight) {
		Vector<String> list = new Vector<String>();
		if (type==Type.RENAME_BV) {
			for (String bvName : project.BVs.keySet()) {
				if (filter.matches(bvName))
					if (highlight)
						list.add(filter.getHighlightedText(bvName));
					else
						list.add(bvName);
			}
		}
		else {
			List<String> bvList = parentAction.getFilterResults(project);
			for(String bv: bvList) 
				switch (type) {
					case CHANGE_MU:		Vector<String> mus = new Vector<String>();
										for(List<Task> taskList : project.tasks.get(bv).values())
											for(Task task : taskList) {
												String mu = task.getMuName();
												if (mu!=null && mu.length()>0)
													if (!mus.contains(mu))
														mus.add(mu);
											}
										for(String mu : mus) 
											if (filter.matches(mu))
												if (highlight)
													list.add(filter.getHighlightedText(mu));
												else
													list.add(mu);
										break;
					case CHANGE_USER:	Vector<String> users = new Vector<String>();
										for(List<Task> taskList : project.tasks.get(bv).values())
											for(Task task : taskList) {
												String u = task.getUserName();
												if (u!=null && u.length()>0)
													if (!users.contains(u))
														users.add(u);
											}
										for(String u : users) 
											if (filter.matches(u))
												if (highlight)
													list.add(filter.getHighlightedText(u));
												else
													list.add(u);
										break;
					case RENAME_TASK:	for(String task : project.tasks.get(bv).keySet()) 
											if (filter.matches(task))
												if (highlight)
													list.add(filter.getHighlightedText(task));
												else
													list.add(task);
				}				
		}
		
		return list;
	} 
	
	public Vector<String> getFilterResults(BVProject project) {
		return getFilterResults(project, false);
	}
	
}

