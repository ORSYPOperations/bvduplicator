package com.orsyp.tools.bvanalyser.model;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.orsyp.tools.bvanalyser.gui.Styles;

public class FilterData  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum Type {EQUALS,STARTS,ENDS,CONTAINS,REGEXP};
	
	public Type type = Type.EQUALS;
	
	public String text = "";

	public FilterData(Type type, String text) {
		this.type = type;
		this.text = text;
	}

	public boolean matches(String task) {
		switch (type) {
			case EQUALS: 	return task.equals(text);
			case STARTS:	return task.startsWith(text);
			case ENDS:		return task.endsWith(text);
			case CONTAINS:	return task.contains(text);
			case REGEXP:	Pattern pattern = Pattern.compile(text.replace("\\", "\\\\"));
					        Matcher matcher = pattern.matcher(task);					    	
					        return matcher.find();
		}
		return false;
	}

	public String getDescription() {
		switch (type) {
			case EQUALS: 	return "Equals '" + text + "'";
			case STARTS: 	return "Starts with '" + text + "'";
			case ENDS:		return "Ends with '" + text + "'";
			case CONTAINS:	return "Contains '" + text + "'";
			case REGEXP:	return "Matches expression '" + text + "'";
		}
		return "No filter";
	}
	
	
	public String getHighlightedText(String s) {		
		switch (type) {
			case EQUALS: 	if (s.equals(text))
								return "<html>"+Styles.HL+s+Styles.HL_END+"</html>";
							else
								return s;
			case STARTS:	
			case ENDS:		
			case CONTAINS:	return "<html>"+s.replace(text,Styles.HL+text+Styles.HL_END)+"</html>";
			case REGEXP:	Pattern pattern = Pattern.compile(text.replace("\\", "\\\\"));
					        Matcher matcher = pattern.matcher(s);					    	
					        if (matcher.find()) 
					        	return "<html>" +
					        						s.substring(0, matcher.start()) + 
					        						Styles.HL +
					        						matcher.group(0) +
					        						Styles.HL_END + 
					        						s.substring(matcher.end()) + 
					        						"</html>";
					        
		}
		return s;
	}
	

}
