package com.orsyp.tools.bvanalyser.model;

import java.io.Serializable;

import com.orsyp.api.task.Task;

public class TaskObj  implements Serializable {
	private static final long serialVersionUID = 1L;

	private Task task;
	
	public TaskObj(Task task) {
		super();
		this.task = task;
	}

	public String getName() {
		if (task!=null)
			return task.getIdentifier().getName();
		return "";
	}
	
	public Task getTask() {
		return task;
	}
}
