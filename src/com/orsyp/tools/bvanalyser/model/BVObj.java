package com.orsyp.tools.bvanalyser.model;

import java.io.Serializable;
import java.util.List;

import com.orsyp.api.bv.BusinessView;
import com.orsyp.api.task.Task;

public class BVObj  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BusinessView bv;
	private List<Task> tasks;
	
	public BVObj(BusinessView bv, List<Task> tasks) {
		super();
		this.bv = bv;
		this.tasks = tasks;
	}	
	
	public String getName() {
		if (bv!=null)
			return bv.getName().replaceAll("\\.BV", "");
		return "";
	}

	public String getLabel() {
		if (bv!=null)
			return bv.getLabel();
		return "";
	}

	public List<Task> getTasks() {
		return tasks;
	}

}
