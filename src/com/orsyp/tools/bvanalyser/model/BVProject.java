package com.orsyp.tools.bvanalyser.model;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/*
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
 */
import com.orsyp.api.bv.BusinessView;
import com.orsyp.api.task.Task;

public class BVProject implements Serializable {
	private static final long serialVersionUID = 1L;

	public TreeMap<String,BusinessView> BVs = new TreeMap<String,BusinessView>(); 
	public TreeMap<String,TreeMap<String,List<Task>>> tasks = new TreeMap<String,TreeMap<String,List<Task>>>();
	
	public ArrayList<Action> actions = new  ArrayList<Action>(); 

	public ConnectionParams sourceConnection = new ConnectionParams();
	public ConnectionParams targetConnection = new ConnectionParams();
	
	public void clearData() {
		BVs.clear();
		tasks.clear();		
	}

	public boolean saveToFile(String filename) {
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			GZIPOutputStream gz = new GZIPOutputStream(fos);
			BufferedOutputStream buf = new BufferedOutputStream(gz);
			
			ObjectOutputStream oos = new ObjectOutputStream(buf);			
			oos.writeObject(this);
			oos.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return false;
		
		
		/*try {
			FileOutputStream fos = new FileOutputStream(filename);
			GZIPOutputStream gz = new GZIPOutputStream(fos);
			BufferedOutputStream buf = new BufferedOutputStream(gz);
			
							
			Output output = new Output(buf);
			new Kryo().writeObject(output, this);
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true; */
	}

	public static BVProject loadFromFile(String filename) {
		try {
			FileInputStream fos = new FileInputStream(filename);
			GZIPInputStream gz = new GZIPInputStream(fos);
			BufferedInputStream buf = new BufferedInputStream(gz);
			
			ObjectInputStream oos = new ObjectInputStream(buf);			
			return (BVProject) oos.readObject();					
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return null;
		
		/*try {
			FileInputStream fos = new FileInputStream(filename);
			GZIPInputStream gz = new GZIPInputStream(fos);
			BufferedInputStream buf = new BufferedInputStream(gz);
			
			try {
				Input input = new Input(buf);
				BVProject prj =  new Kryo().readObject(input, BVProject.class);
				input.close();
				if (prj!=null)
					return prj;
			} 
			catch (Exception e) {
				e.printStackTrace();
				//for backward compatibility
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return null; */
	}
}
