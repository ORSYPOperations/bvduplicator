package com.orsyp.tools.bvanalyser.model;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.orsyp.tools.bvanalyser.gui.Styles;

public class RenameData  implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum Type {NOCHANGE,SET,PREFIX,SUFFIX,REGEXP};
	
	public Type type = Type.NOCHANGE;
	
	public String text = "";
	public String replacement = "";
	
	public RenameData(Type type, String text, String replacement) {
		this.type = type;
		this.text = text;
		this.replacement = replacement;
	}

	public String getNewName(String name) {
		return getNewName(name,false);
	}
	
	public String getNewName(String name, boolean highlight) {
		switch (type) {
			case NOCHANGE:	return name;
			case SET:		if (highlight)
								return "<html>" + Styles.HL + text + Styles.HL_END + "</html>";
							else
								return text;
			case PREFIX:	if (highlight)
								return "<html>" + Styles.HL + text + Styles.HL_END + name + "</html>";
							else
								return text + name;
			case SUFFIX:	if (highlight)
								return "<html>" + name + Styles.HL + text + Styles.HL_END + "</html>";
							else 
								return name + text;
			case REGEXP:	Pattern pattern = Pattern.compile(text.replace("\\", "\\\\"));
					        Matcher matcher = pattern.matcher(name);
					    	
					        if (matcher.find()) {
					        	if (highlight)
					        		return "<html>" +
				        					name.substring(0, matcher.start()) + 
				        					Styles.HL +
				        					replacement +
				        					Styles.HL_END + 
				        					name.substring(matcher.end()) + 
				        					"</html>";
					        	else
					        		return name.substring(0, matcher.start()) + 
			        						replacement +
			        						name.substring(matcher.end());
					        }
					        else
					        	return name;
		}
		return "";
	}
	
	public String getDescription() {
		switch (type) {
			case NOCHANGE:	return "No change";
			case SET:		return "Set to '" + text + "'";
			case PREFIX:	return "Add prefix '" + text + "'";
			case SUFFIX:	return "Add suffix '" + text + "'";
			case REGEXP:	return "Replace '" + text + "' with '" + replacement + "'";
		}
		return "No renaming";
	}
	
	
	/*
	 * String tx = txRename.getText();
	int row=0;
	for (String bv : filterMatches) {
		bv=bv.replace("<html>", "").replace("</html>", "").replace(STYLE, "").replace(STYLE_END, "");
		modelData[row][0]=bv;
		if (rbNameNoChange.isSelected()) 
			modelData[row][1]=bv;
		else
		if (rbNameSet.isSelected()) 
			modelData[row][1]=tx;
		else
		if (rbNamePrefix.isSelected()) 
			modelData[row][1]=tx+bv;
		else
		if (rbNameSuffix.isSelected()) 
			modelData[row][1]=bv+tx;
		else
		if (rbNameReplace.isSelected()) {
			try {
				String from = txReplaceFrom.getText();
				String to = txReplaceTo.getText();
				Pattern pattern = Pattern.compile(from.replace("\\", "\\\\"));
		        Matcher matcher = pattern.matcher(bv);

		        if (matcher.find()) {
		        	modelData[row][1] = "<html>" +
		        						bv.substring(0, matcher.start()) + 
		        						STYLE +
		        						to +
		        						STYLE_END + 
		        						bv.substring(matcher.end()) + 
		        						"</html>";
		        }
		        else
		        	modelData[row][1] = bv;
			}
			catch(Exception e) {
				modelData[row][1] = bv;
			}
		}
		row++;
	}
		
	*/

}
