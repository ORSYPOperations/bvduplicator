package com.orsyp.tools.bvanalyser.model;

import java.io.Serializable;

public class ConnectionParams  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String host;
	public String port;
	public String user;
	public String pw;
	public String node;
	public String area;	
}
