package com.orsyp.tools.bvanalyser;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.jdesktop.swingx.JXTreeTable;

import com.orsyp.api.ObjectAlreadyExistException;
import com.orsyp.api.bv.BusinessView;
import com.orsyp.api.task.Task;
import com.orsyp.tools.bvanalyser.api.DuApiConnection;
import com.orsyp.tools.bvanalyser.gui.BVActionForm;
import com.orsyp.tools.bvanalyser.gui.ConnectionDialog;
import com.orsyp.tools.bvanalyser.gui.ErrorDlg;
import com.orsyp.tools.bvanalyser.gui.Styles;
import com.orsyp.tools.bvanalyser.gui.actiontree.ActionTableCellRenderer;
import com.orsyp.tools.bvanalyser.gui.actiontree.ActionTreeCellRenderer;
import com.orsyp.tools.bvanalyser.gui.actiontree.ActionTreeItem;
import com.orsyp.tools.bvanalyser.gui.actiontree.ActionTreeModel;
import com.orsyp.tools.bvanalyser.gui.bvtree.BVTreeItem;
import com.orsyp.tools.bvanalyser.gui.bvtree.BVTreeModel;
import com.orsyp.tools.bvanalyser.gui.bvtree.BvTableCellRenderer;
import com.orsyp.tools.bvanalyser.gui.bvtree.BvTreeCellRenderer;
import com.orsyp.tools.bvanalyser.gui.bvtree.BVTreeItem.Type;
import com.orsyp.tools.bvanalyser.gui.util.CustomFileFilter;
import com.orsyp.tools.bvanalyser.model.Action;
import com.orsyp.tools.bvanalyser.model.BVObj;
import com.orsyp.tools.bvanalyser.model.BVProject;
import com.orsyp.tools.bvanalyser.model.TaskObj;
import com.orsyp.util.io.Lines;

import bibliothek.extension.gui.dock.theme.EclipseTheme;
import bibliothek.gui.dock.common.CControl;
import bibliothek.gui.dock.common.CGrid;
import bibliothek.gui.dock.common.DefaultSingleCDockable;
import bibliothek.gui.dock.common.action.CAction;
import bibliothek.gui.dock.common.action.CButton;

public class BVDuplicator extends JFrame {
	private static final long serialVersionUID = 1L;

	private String versionNumber = "V1.0";
	
	private BVProject project = new BVProject();
	
	private boolean unsavedProject = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					installLnF();
					BVDuplicator frame = new BVDuplicator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private static final String PREFERRED_LOOK_AND_FEEL = "com.nilo.plaf.nimrod.NimRODLookAndFeel";
	
	private static void installLnF() {
		try {
			String lnfClassname = PREFERRED_LOOK_AND_FEEL;
			UIManager.setLookAndFeel(lnfClassname);
		} catch (Exception e) {
			System.err.println("Cannot install " + PREFERRED_LOOK_AND_FEEL + " on this platform:" + e.getMessage());
		}
	}

	
	private JXTreeTable bvTree;
	private JXTreeTable actionTree;


	/**
	 * Create the frame.
	 */
	public BVDuplicator() {		
		addDocks();
		setJMenuBar(getMainMenuBar());		
		setSize(1000, 720);
		setTitle("ORSYP BV Duplicator - " + versionNumber );
		setIconImage(new ImageIcon("img/du.png").getImage());	
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		getContentPane().setPreferredSize(getSize());
		pack();
		setLocationRelativeTo(null);
		
		addWindowListener(new WindowAdapter() {
								@Override
								public void windowOpened(WindowEvent e) {
									ShowStartup();
								}

								@Override
								public void windowClosing(WindowEvent e) {
									if (askSaveProject(true))
										System.exit(0);
								}
							} ); 
		
		//debug default values
		project.sourceConnection.node = "itlpmps01"; 
		project.sourceConnection.area = "X";
		project.sourceConnection.host = "localhost";
		project.sourceConnection.port = "4184";
		project.sourceConnection.user = "admin";
		project.sourceConnection.pw = "admin";
	}

	
	@SuppressWarnings("deprecation")
	private void addDocks() {
		CControl control = new CControl( this );
		control.getContentArea().remove( control.getContentArea().getEastArea());
		control.getContentArea().remove( control.getContentArea().getWestArea());
		control.getContentArea().remove( control.getContentArea().getNorthArea());
		control.getContentArea().remove( control.getContentArea().getSouthArea());
		control.setTheme(  new EclipseTheme() );		
		this.add( control.getContentArea() );
		
		CAction act;
		CGrid grid = new CGrid( control );
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(getBvTreePanel());
		
		DefaultSingleCDockable dk = create( "Business Views", scrollPane);
		
		act = new CButton("Get Business Views from $U node...", new ImageIcon("img/server.png")) {
			@Override 
			protected void action() { 								
				getBVs();
			}};		
		dk.addAction(act);
		
		dk.addSeparator();
		
		act = new CButton("Create new Business Views...", new ImageIcon("img/duplicate.png")) {
			@Override 
			protected void action() { 								
				Vector<String> errors = duplicateBVs();
				
				if (errors.size()>0) 
					new ErrorDlg(errors).setVisible(true);
				else
					JOptionPane.showMessageDialog(null, "New objects created successfully", "Info", JOptionPane.INFORMATION_MESSAGE);
				
				loadData(project.sourceConnection.node, project.sourceConnection.area, project.sourceConnection.host, Integer.parseInt(project.sourceConnection.port), project.sourceConnection.user, project.sourceConnection.pw);
				unsavedProject = true;
				buildBVTree();		
				buildActionTree();
					
			}};		
		dk.addAction(act);
		
		dk.addSeparator();
		
		act = new CButton("Expand all", new ImageIcon("img/expand_all.png")) {
			@Override 
			protected void action() { 								
				expandAllTree();
			}};		
		dk.addAction(act);
		act = new CButton("Collapse all", new ImageIcon("img/collapse_all.png")) {
					@Override 
					protected void action() { 								
						collapseAllTree();
					}};		
		dk.addAction(act);
		grid.add( 0, 0, 1, 1, dk );		
	
		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setViewportView(getActionTreePanel());
		
		DefaultSingleCDockable actionDock = create( "Actions", scrollPane2 );
		act = new CButton("Add Business View rename action", new ImageIcon("img/rename_bv.png")) {
							@Override
							protected void action() {
								newAction(Action.Type.RENAME_BV,null);
							}};
							actionDock.addAction(act);
		act = new CButton("Add Task rename action...", new ImageIcon("img/rename_task.png")) {
							@Override
							protected void action() {
								Action act = getBVActionFromSelectedAction();
								if (act==null)
									JOptionPane.showMessageDialog(null, "Select a parent Business View action", "Warning", JOptionPane.ERROR_MESSAGE);	
								else 
									newAction(Action.Type.RENAME_TASK,act);								
							}};		
		actionDock.addAction(act);			
		act = new CButton("Add change user action...", new ImageIcon("img/user.png")) {
							@Override
							protected void action() {
								Action act = getBVActionFromSelectedAction();
								if (act==null)
									JOptionPane.showMessageDialog(null, "Select a parent Business View action", "Warning", JOptionPane.ERROR_MESSAGE);	
								else {
									boolean hasTaskChange = false;
									for (Action a : project.actions) 
										if (a.parentAction == act)
											if (a.type == Action.Type.CHANGE_MU || a.type == Action.Type.RENAME_TASK) {
												hasTaskChange = true;
												break;
											}
									if (hasTaskChange)
										newAction(Action.Type.CHANGE_USER,act);
									else
										JOptionPane.showMessageDialog(null, "Can't change the user on original tasks. Create an action to change tasks before changing the user.", "Warning", JOptionPane.ERROR_MESSAGE);
								}
							}};		
		actionDock.addAction(act);
		act = new CButton("Add change MU action...", new ImageIcon("img/mu.png")) {
							@Override
							protected void action() {
								Action act = getBVActionFromSelectedAction();
								if (act==null)
									JOptionPane.showMessageDialog(null, "Select a parent Business View action", "Warning", JOptionPane.ERROR_MESSAGE);	
								else
									newAction(Action.Type.CHANGE_MU,act);	
							}};		
		actionDock.addAction(act);
		
		actionDock.addSeparator();
		
		/*
		act = new CButton("Duplicate and modify action...", new ImageIcon("img/copy.png")) {
			@Override
			protected void action() {
				//TODO duplicate
			}};	
		actionDock.addAction(act);
		*/
		
		act = new CButton("Delete selected actions", new ImageIcon("img/delete.png")) {
			@Override
			protected void action() {
				deleteAction();
			}};		
		actionDock.addAction(act);
		grid.add( 0, 1, 1, 1, actionDock );
		
		control.getContentArea().deploy( grid );
	}	



	protected void newAction(Action.Type type, Action parentAction) {
		BVActionForm form = new BVActionForm(project,type, parentAction);
		form.setVisible(true);
		if (form.isAccepted()) {
			project.actions.add(form.getAction());
			buildActionTree();
			bvTree.updateUI();
			unsavedProject = true;
		}
		
	}


	private DefaultSingleCDockable create( String title, JComponent comp ){
		DefaultSingleCDockable dockable = new DefaultSingleCDockable( title, title );
		dockable.setTitleIcon(null);
		dockable.setExternalizable(false);
		dockable.setMinimizable(false);
		dockable.setTitleText( title );
		dockable.setCloseable( false );		
		dockable.setMaximizable(false );
		dockable.add( comp );
		return dockable;
	}


	protected void collapseAllTree() {
		bvTree.collapseAll();
	}


	protected void expandAllTree() {
		bvTree.expandAll();
	}


	private JXTreeTable getBvTreePanel() {
		if (bvTree == null) {
			bvTree = new JXTreeTable();
			bvTree.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			bvTree.setColumnControlVisible(false);
			bvTree.setRootVisible(true);
			bvTree.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			bvTree.setHorizontalScrollEnabled(true);
			bvTree.setFillsViewportHeight(false);
			bvTree.setDefaultRenderer(Object.class, new BvTableCellRenderer(this));
			bvTree.setTreeCellRenderer(new BvTreeCellRenderer());			
		}
		return bvTree;
	}
	
	private JXTreeTable getActionTreePanel() {
		if (actionTree == null) {
			actionTree = new JXTreeTable();
			actionTree.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			actionTree.setColumnControlVisible(false);
			actionTree.setRootVisible(false);
			actionTree.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			actionTree.setHorizontalScrollEnabled(true);
			actionTree.setFillsViewportHeight(false);
			actionTree.setDefaultRenderer(Object.class, new ActionTableCellRenderer());
			actionTree.setTreeCellRenderer(new ActionTreeCellRenderer());			
		}
		return actionTree;
	}


	private JMenuBar getMainMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		
		JMenu mnFile = new JMenu("Project");
		menuBar.add(mnFile);
		
		JMenuItem mntmNewProject = new JMenuItem("New project...");
		mnFile.add(mntmNewProject);
		mntmNewProject.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (askSaveProject(true))
					newProject();				
			}
		});
		
		JSeparator sep1 = new JSeparator();
		mnFile.add(sep1);
		
		JMenuItem mntmOpenProject = new JMenuItem("Open project...");
		mnFile.add(mntmOpenProject);
		mntmOpenProject.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (askSaveProject(true))
					loadProject();				
			}
		});
		
		JMenuItem mntmSaveProject = new JMenuItem("Save project...");
		mnFile.add(mntmSaveProject);
		mntmSaveProject.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveProject();				
			}
		});
		
		JSeparator sep2 = new JSeparator();
		mnFile.add(sep2);
		
		JMenuItem mnExit = new JMenuItem("Exit");
		mnFile.add(mnExit);
		mnExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (askSaveProject(true))
					System.exit(0);				
			}
		});
		
		return menuBar;
	}

	protected Action getBVActionFromSelectedAction() {
		int i = actionTree.getSelectionModel().getLeadSelectionIndex();
		if (i>=0) {
			ActionTreeItem it = (ActionTreeItem)actionTree.getModel().getValueAt(i, 0);
			if (it.getParent().action!=null)
				it = it.getParent();
			return it.action;
		}
		return null;
	}

	// BVs ----------------------------
	
	protected void getBVs() {
		ConnectionDialog dlg = new ConnectionDialog(project.sourceConnection.node, 
													project.sourceConnection.area, 
													project.sourceConnection.host,
													project.sourceConnection.port,
													project.sourceConnection.user,
													project.sourceConnection.pw);
		dlg.setVisible(true);
		if (!dlg.isAccepted())
			return;
		
		project.sourceConnection.host = dlg.getHost();
		project.sourceConnection.port = String.valueOf(dlg.getPort());
		project.sourceConnection.user = dlg.getUser();
		project.sourceConnection.pw = dlg.getPassword();
		project.sourceConnection.node = dlg.getNode();		
		project.sourceConnection.area = dlg.getArea();		
		
		loadData(dlg.getNode(), dlg.getArea(), dlg.getHost(), dlg.getPort(), dlg.getUser(), dlg.getPassword());		
				
		unsavedProject = true;
		buildBVTree();		
		buildActionTree();
		
		if (project.sourceConnection.node==null) {
			project.targetConnection.node = project.sourceConnection.node; 
			project.targetConnection.area = project.sourceConnection.area;
			project.targetConnection.host = project.sourceConnection.host;
			project.targetConnection.port = project.sourceConnection.port;
			project.targetConnection.user = project.sourceConnection.user;
			project.targetConnection.pw = project.sourceConnection.pw;
		}
		
	}
	
	protected void loadData(String node, String area, String host, int port, String user, String pw) {
		DuApiConnection conn = new DuApiConnection(node, area, host, port , user, pw);		
		project.clearData();		
		
		List<String> bvs = conn.getBVList();
		for (String bvName : bvs) {			
			String bvNameNoExt = bvName.substring(0,bvName.length()-3);
			BusinessView bv = conn.getBV(bvName);
			project.BVs.put(bvNameNoExt, bv);
			TreeMap<String,List<Task>> tt = new TreeMap<String,List<Task>>();
			project.tasks.put(bvNameNoExt, tt);
												
			for (Task task : conn.getBVTasks(bv)) {
				String taskName = task.getIdentifier().getName();
				List<Task> list = tt.get(taskName);
				if (list==null) {
					list = new ArrayList<Task>();
					tt.put(taskName, list);
				}
				list.add(task);
			}
		}		
	}
	
	protected Vector<String> duplicateBVs() {
		Vector<String> errors = new Vector<String>(); 
		ConnectionDialog dlg = new ConnectionDialog(project.targetConnection.node, 
													project.targetConnection.area, 
													project.targetConnection.host,
													project.targetConnection.port,
													project.targetConnection.user,
													project.targetConnection.pw);
		dlg.setVisible(true);
		if (!dlg.isAccepted())
			return errors;
		
		project.targetConnection.host = dlg.getHost();
		project.targetConnection.port = String.valueOf(dlg.getPort());
		project.targetConnection.user = dlg.getUser();
		project.targetConnection.pw = dlg.getPassword();
		project.targetConnection.node = dlg.getNode();		
		project.targetConnection.area = dlg.getArea();
		
		//connect to target $U node
		DuApiConnection conn = new DuApiConnection(dlg.getNode(), dlg.getArea(), dlg.getHost(), dlg.getPort(), dlg.getUser(), dlg.getPassword());
	
		//get existing users and mus
		List<String> users = null;
		List<String> mus = null;		
		try {
			users = conn.getUsers();
			mus = conn.getMus();
		} catch (Exception e1) {
			e1.printStackTrace();
			errors.add(getErrorString(e1.getMessage()));
			return errors;
		}
		
		//cycle BVs
		for (String bvName: project.BVs.keySet())
			//check BV actions
			for (Action a: project.actions)
				if (a.type==Action.Type.RENAME_BV)
					//if action applies to the current BV
					if (a.filter.matches(bvName)) {
						//System.out.println("BV: " + bvName + " --- " + a.getName() + " - " + a.getFilterString() + " - " + a.getRenameString());
						
						//get new name
						String newBvName = a.rename.getNewName(bvName);
						String newXML ="";
						boolean xmlChanged = false;
						for (String l : project.BVs.get(bvName).getData().getLines()) 
							newXML += l.replace(">", ">\n");
						String[] newXMLLines = newXML.split("\\n");
						
						//cycle BV tasks  
						TreeMap<String,List<Task>> tm = project.tasks.get(bvName);
						for (List<Task> taskList : tm.values()) 
							for (Task task : taskList) {							
								String taskName = task.getIdentifier().getName();
								String taskUser = task.getUserName();
								String taskMU = task.getMuName();
								String newTaskName = null;
								String newUser = null;
								String newMU = null;
								
								//cycle actions on tasks in BVs selected by parent action
								for (Action aTask: project.actions)
									if (aTask.parentAction == a)
										//if the action matches, perform the renaming on the related object
										switch (aTask.type) {
											case RENAME_TASK:	if (aTask.filter.matches(taskName)) 
																	newTaskName = aTask.rename.getNewName(taskName);
																break;
											case CHANGE_USER:	if (aTask.filter.matches(taskUser)) 
																	newUser = aTask.rename.getNewName(taskUser);
																break;
											case CHANGE_MU:		if (aTask.filter.matches(taskMU)) 
																	newMU = aTask.rename.getNewName(taskMU);
																break;
										}
								
								//copy task
								if (newTaskName!=null || newMU!=null) {
									
									boolean err = false;
									if (newUser!= null && !users.contains(newUser)) {
										String t = newTaskName;
										if (t==null)
											t=taskName;
										errors.add(getErrorString("Can't set new user on task '"+t+"': user '"+newUser+"' does not exist in target $U node"));
										err=true;
									}
									if (newMU!= null && !mus.contains(newMU)) {
										String t = newTaskName;
										if (t==null)
											t=taskName;
										errors.add(getWarningString("Can't set new MU on task '"+t+"': MU '"+newMU+"' does not exist in target $U node"));
										err=true;
									}
									
									if (!err) { 				
										//System.out.println("Task: " + task.getIdentifier().getName() + " > " + newTaskName + " - " + newUser +" - "+ newMU);
										//copy the task
										try {
											conn.copyTask(task, newTaskName, newUser, newMU);
										} catch (Exception e) {
											e.printStackTrace();
											String t = newTaskName;
											if (t==null)
												t=taskName;
											if (e instanceof ObjectAlreadyExistException) {
												String muStr = newMU;
												if (muStr==null)
													muStr = taskMU;
												if (muStr==null || muStr.length()==0)
													muStr = "";
												else
													muStr ="with MU '"+ muStr+"' "; 
												errors.add(getWarningString("Task '"+t+"' "+ muStr+"already exists"));
											}
											else
												errors.add(getErrorString("Copying task '"+taskName+"' to '"+t+"': " + e.getMessage()));											
										}
										
										for (int i = 0; i<newXMLLines.length; i++) {
											String line = newXMLLines[i];											
											if (line.contains("taskName=\""+taskName+"\"") && line.contains("templateName=\""+taskMU+"\"")) {
												if (newTaskName!=null)
													line = line.replace("taskName=\""+taskName+"\"", "taskName=\""+newTaskName+"\"");
												if (newMU!=null)
													line = line.replace("templateName=\""+taskMU+"\"", "templateName=\""+newMU+"\"");
												newXMLLines[i]=line;
											}
										}
										xmlChanged = true;
									}
								}
							}
						
						//replace task references in BV xml
						Lines newLines = null;
						if (xmlChanged) {
							newLines = new Lines();
							for (String l : newXMLLines) 
								if (l.length()>0)
									newLines.add(l);
						}
						
						//copy BV
						try {
							conn.copyBV(project.BVs.get(bvName), newBvName+".BV", newLines);
						} catch (Exception e) {
							e.printStackTrace();
							errors.add(getErrorString("Copying Business View '"+bvName+"' to '"+newBvName+"': " + e.getMessage()));
						}
					}

		return errors;
	}
	
	private String getErrorString(String message) {
		return "<html>"+Styles.ERRLOG + "ERROR" + Styles.ERRLOG_END + " " +message + "<html>";
	}
	
	private String getWarningString(String message) {
		return "<html>"+Styles.WARNLOG + "WARNING" + Styles.WARNLOG_END + " " +message + "<html>";
	}


	private void buildBVTree() {
		List<BVTreeItem> items = new ArrayList<BVTreeItem>();
		for (String bvName: project.BVs.keySet()) {
			BusinessView bv =  project.BVs.get(bvName);
			ArrayList<Task> taskList = new ArrayList<Task>();
			for (List<Task> l : project.tasks.get(bvName).values()) 
				taskList.addAll(l);
			BVObj bvObj =  new BVObj(bv,taskList);			
			BVTreeItem bvTreeItem = new BVTreeItem(Type.BV, null, bvObj);
			items.add(bvTreeItem);
			for (List<Task> list : project.tasks.get(bvName).values()) 
				for (Task task : list) {
					TaskObj taskObj =  new TaskObj(task);
					BVTreeItem item = new BVTreeItem(Type.TASK, bvTreeItem, taskObj);
					items.add(item);
				}
		}
		
		bvTree.setRootVisible(items.size()>0);
		
		BVTreeModel model = new BVTreeModel(items,project.sourceConnection.node);
		bvTree.setTreeTableModel(model);
		bvTree.expandAll();		
	}

	//actions -----------------------------
	
	private void buildActionTree() {	
		List<ActionTreeItem> actItems = new ArrayList<ActionTreeItem>();
		
		for (Action a: project.actions) {
			if (a.type==Action.Type.RENAME_BV) {
				ActionTreeItem item = new ActionTreeItem(ActionTreeItem.Type.BV, null, a);
				actItems.add(item);
				for (Action childAct: project.actions) 
					if (childAct.parentAction==a)
						actItems.add(new ActionTreeItem(ActionTreeItem.Type.TASK, item, childAct));		
			}
		}
		
		ActionTreeModel actModel = new ActionTreeModel(actItems);
		actionTree.setTreeTableModel(actModel);
		actionTree.expandAll();
	}
	
	// other gui commands ----------------
	
	protected void deleteAction() {
		int i = actionTree.getSelectionModel().getLeadSelectionIndex();
		if (i>=0) {
			ActionTreeItem it = (ActionTreeItem)actionTree.getModel().getValueAt(i, 0);
			if (it.action.type==Action.Type.RENAME_BV)
				for (int idx = project.actions.size()-1; idx>=0; idx--) 
					if (project.actions.get(idx).parentAction==it.action)
						project.actions.remove(idx);
			project.actions.remove(it.action);
			unsavedProject = true;
			buildActionTree();
			bvTree.updateUI();
		}
		
	}	
	
	protected boolean askSaveProject(boolean canCancel) {
		if (project!=null)
			if (unsavedProject) {
				int ret = -1;
				if (canCancel)
					ret = JOptionPane.showConfirmDialog(this, "Save changes to current project?", "Save changes", JOptionPane.YES_NO_CANCEL_OPTION);
				else
					ret = JOptionPane.showConfirmDialog(this, "Save changes to current project?", "Save changes", JOptionPane.YES_NO_OPTION);
				
				if ((ret==JOptionPane.CANCEL_OPTION) || (ret==JOptionPane.CLOSED_OPTION))
					return false;
				
				if (ret==JOptionPane.YES_OPTION) 
					saveProject();
			}
				
		return true;
	}
	
	protected void newProject() {
		project = new BVProject();
		buildBVTree();
		buildActionTree();
		unsavedProject = false;
	}


	protected void saveProject() {
		JFileChooser c = new JFileChooser();
		c.addChoosableFileFilter(new CustomFileFilter(CustomFileFilter.Type.DUP));
	    int rVal = c.showSaveDialog(this);
	    if (rVal == JFileChooser.APPROVE_OPTION) {
	    	String filename = c.getSelectedFile().getPath();
	    	if (!filename.endsWith(".bvdup"))
	    		filename += ".bvdup";
	    	project.saveToFile(filename);
	    	unsavedProject = false;
	    }		
		
	}


	protected void loadProject() {
		JFileChooser c = new JFileChooser();
		c.addChoosableFileFilter(new CustomFileFilter(CustomFileFilter.Type.DUP));
	    int rVal = c.showOpenDialog(this);
	    if (rVal == JFileChooser.APPROVE_OPTION) { 
	    	project = BVProject.loadFromFile(c.getSelectedFile().getPath());
	    	buildBVTree();
			buildActionTree();
			unsavedProject = false;
	    }
	}	

	protected void ShowStartup() {
		// TODO startup form - optional
	}


	public BVProject getProject() {
		return project;
	}
	
}
