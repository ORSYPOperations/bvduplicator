package com.orsyp.tools.bvanalyser.gui;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Dimension;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.orsyp.tools.bvanalyser.model.Action;
import com.orsyp.tools.bvanalyser.model.BVProject;
import com.orsyp.tools.bvanalyser.model.FilterData;
import com.orsyp.tools.bvanalyser.model.RenameData;

public class BVActionForm extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private ButtonGroup buttonGroup = new ButtonGroup();
	private ButtonGroup buttonGroup_1 = new ButtonGroup();
	
	private JTextField txFilter;
	private JTextField txRename;
	private JTable table;
	private CardLayout filterCardLayout;
	private JPanel filterCardPanel;
	private CardLayout replaceCardLayout;
	private JPanel replaceCardPanel;
	
	private JRadioButton rbFilterEquals;
	private JRadioButton rbFilterStarts;
	private JRadioButton rbFilterEnds;
	private JRadioButton rbFilterContains;
	private JRadioButton rbFilterRegExpr;
	
	private JList lsFilterPreview;
	
	private JRadioButton rbNameSet;
	private JRadioButton rbNameSuffix;
	private JRadioButton rbNamePrefix;
	private JRadioButton rbNameReplace;
	private JRadioButton rbNameNoChange;
	
	private JTextField txReplaceFrom;
	private JTextField txReplaceTo;
	
	private BVProject project;
	private Action action;
	private boolean accepted = false;


	/**
	 * Create the dialog.
	 */
	public BVActionForm(BVProject project, Action.Type actionType, Action parentAction) {
		this.project = project;
		
		String filterTitle ="";
		String renameTitle ="";		
		switch (actionType) {
			case RENAME_BV: 	setTitle("Duplicate Business Views");
								setIconImage(new ImageIcon("img/rename_bv.png").getImage());
								filterTitle =" Filter on BV name ";
								renameTitle =" Target BV name ";
								break;
			case RENAME_TASK: 	setTitle("Duplicate Tasks");
								setIconImage(new ImageIcon("img/rename_task.png").getImage());
								filterTitle =" Filter on Task name ";
								renameTitle =" Target Task name ";
								break;
			case CHANGE_USER: 	setTitle("Change User");
								setIconImage(new ImageIcon("img/user.png").getImage());
								filterTitle =" Filter users ";
								renameTitle =" Target user name ";
								break;
			case CHANGE_MU: 	setTitle("Change MU");
								setIconImage(new ImageIcon("img/mu.png").getImage());
								filterTitle =" Filter MUs ";
								renameTitle =" Target MU name ";
								break;
		}
				
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 555, 319);
		setMinimumSize(new Dimension(500,300));
		
		filterCardPanel = new JPanel();
		getContentPane().add(filterCardPanel, BorderLayout.CENTER);
		filterCardLayout = new CardLayout(0, 0);
		filterCardPanel.setLayout(filterCardLayout);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new EmptyBorder(8, 8, 0, 8));
		filterCardPanel.add(panel_4, "filter");
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel_4.add(panel, BorderLayout.WEST);
		panel.setPreferredSize(new Dimension(230, 10));
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), filterTitle , TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(0, 0, 220, 163);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		rbFilterEquals = new JRadioButton("Equals");
		rbFilterEquals.setSelected(true);
		buttonGroup.add(rbFilterEquals);
		rbFilterEquals.setBounds(6, 24, 124, 23);
		panel_1.add(rbFilterEquals);
		rbFilterEquals.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent evt) {				
				changeFilterType();
			}
		});
		
		
		rbFilterStarts = new JRadioButton("Starts with");
		buttonGroup.add(rbFilterStarts);
		rbFilterStarts.setBounds(6, 50, 124, 23);
		panel_1.add(rbFilterStarts);
		rbFilterStarts.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent evt) {				
				changeFilterType();
			}
		});
		
		rbFilterEnds = new JRadioButton("Ends with");
		buttonGroup.add(rbFilterEnds);
		rbFilterEnds.setBounds(6, 76, 124, 23);
		panel_1.add(rbFilterEnds);
		rbFilterEnds.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent evt) {				
				changeFilterType();
			}
		});
		
		rbFilterContains = new JRadioButton("Contains");
		buttonGroup.add(rbFilterContains);
		rbFilterContains.setBounds(6, 102, 124, 23);
		panel_1.add(rbFilterContains);
		rbFilterContains.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent evt) {				
				changeFilterType();
			}
		});
		
		rbFilterRegExpr = new JRadioButton("Matches regular expression");
		buttonGroup.add(rbFilterRegExpr);
		rbFilterRegExpr.setBounds(6, 128, 208, 23);
		panel_1.add(rbFilterRegExpr);
		rbFilterRegExpr.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent evt) {				
				changeFilterType();
			}
		});
		
		JLabel lblValue = new JLabel("Value");
		lblValue.setBounds(0, 174, 46, 14);
		panel.add(lblValue);
		
		txFilter = new JTextField();
		txFilter.setBounds(0, 192, 220, 24);
		panel.add(txFilter);
		txFilter.setColumns(10);
		txFilter.getDocument().addDocumentListener(new DocumentListener() {		
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				updateFilterPreview();
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				updateFilterPreview();
			}
			@Override
			public void changedUpdate(DocumentEvent arg0) {
				updateFilterPreview();				
			}
		});
		
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5, BorderLayout.CENTER);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel("Preview");
		lblNewLabel.setPreferredSize(new Dimension(38, 18));
		panel_5.add(lblNewLabel, BorderLayout.NORTH);
		
		JScrollPane scrollPane = new JScrollPane();
		panel_5.add(scrollPane, BorderLayout.CENTER);
		
		lsFilterPreview = new JList();
		scrollPane.setViewportView(lsFilterPreview);
		
		JPanel panel_11 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_11.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panel_4.add(panel_11, BorderLayout.SOUTH);
		
		JButton btnNewButton = new JButton("Cancel");
		btnNewButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		panel_11.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Set target >>");
		btnNewButton_1.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (lsFilterPreview.getModel().getSize()==0)
					JOptionPane.showMessageDialog(null, "The current filter selects no objects", "Warning", JOptionPane.ERROR_MESSAGE);
				else {
					updateRenamePreview();
					filterCardLayout.show(filterCardPanel, "Rename");
				}
			}
		});
		panel_11.add(btnNewButton_1);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new EmptyBorder(8, 8, 0, 8));
		filterCardPanel.add(panel_6, "Rename");
		panel_6.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(null);
		panel_6.add(panel_7, BorderLayout.CENTER);
		panel_7.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_8.setPreferredSize(new Dimension(230, 10));
		panel_7.add(panel_8, BorderLayout.WEST);
		
		JPanel panel_9 = new JPanel();
		panel_9.setLayout(null);
		panel_9.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), renameTitle, TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_9.setBounds(0, 0, 220, 163);
		panel_8.add(panel_9);
		
		rbNameSet = new JRadioButton("Set new name");		
		buttonGroup_1.add(rbNameSet);
		rbNameSet.setBounds(6, 50, 124, 23);		
		panel_9.add(rbNameSet);
		rbNameSet.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent evt) {				
				changeRenameType();
			}
		});
		
		rbNameSuffix = new JRadioButton("Append suffix");
		buttonGroup_1.add(rbNameSuffix);
		rbNameSuffix.setBounds(6, 102, 124, 23);
		panel_9.add(rbNameSuffix);
		rbNameSuffix.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent evt) {				
				changeRenameType();
			}
		});
		
		rbNamePrefix = new JRadioButton("Prepend prefix");
		buttonGroup_1.add(rbNamePrefix);
		rbNamePrefix.setBounds(6, 76, 124, 23);
		panel_9.add(rbNamePrefix);
		rbNamePrefix.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent evt) {				
				changeRenameType();
			}
		});
		
		rbNameReplace = new JRadioButton("Replace text");
		buttonGroup_1.add(rbNameReplace);
		rbNameReplace.setBounds(6, 128, 151, 23);
		panel_9.add(rbNameReplace);
		rbNameReplace.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent evt) {				
				changeRenameType();
			}
		});
		
		rbNameNoChange = new JRadioButton("No change");
		buttonGroup_1.add(rbNameNoChange);
		rbNameNoChange.setSelected(true);
		rbNameNoChange.setBounds(6, 24, 178, 23);
		panel_9.add(rbNameNoChange);
		rbNameNoChange.addChangeListener(new ChangeListener() {			
			@Override
			public void stateChanged(ChangeEvent evt) {				
				changeRenameType();
			}
		});
		
		replaceCardPanel = new JPanel();
		replaceCardPanel.setBounds(0, 174, 220, 66);
		panel_8.add(replaceCardPanel);
		replaceCardLayout = new CardLayout(0, 0);
		replaceCardPanel.setLayout(replaceCardLayout);
		
		JPanel panel_3 = new JPanel();
		replaceCardPanel.add(panel_3, "new value");
		panel_3.setLayout(null);
		
		txRename = new JTextField();
		txRename.setBounds(0, 20, 220, 24);
		panel_3.add(txRename);
		txRename.setColumns(10);
		txRename.getDocument().addDocumentListener(new RenameDocListener());
		
		JLabel label = new JLabel("Value");
		label.setBounds(0, 0, 46, 14);
		panel_3.add(label);
		
		JPanel panel_12 = new JPanel();
		replaceCardPanel.add(panel_12, "replace");
		panel_12.setLayout(null);
		
		JLabel lblOriginal = new JLabel("Original");
		lblOriginal.setBounds(0, 6, 59, 14);
		panel_12.add(lblOriginal);
		
		txReplaceFrom = new JTextField();
		txReplaceFrom.setColumns(10);
		txReplaceFrom.setBounds(80, 1, 140, 24);
		panel_12.add(txReplaceFrom);
		txReplaceFrom.getDocument().addDocumentListener(new RenameDocListener());
		
		txReplaceTo = new JTextField();
		txReplaceTo.setColumns(10);
		txReplaceTo.setBounds(80, 36, 140, 24);
		panel_12.add(txReplaceTo);
		txReplaceTo.getDocument().addDocumentListener(new RenameDocListener());
		
		JLabel lblNewValue = new JLabel("New value");
		lblNewValue.setBounds(0, 41, 70, 14);
		panel_12.add(lblNewValue);
		
		JPanel panel_13 = new JPanel();
		replaceCardPanel.add(panel_13, "empty");
		replaceCardLayout.show(replaceCardPanel, "empty");
		
		JPanel panel_10 = new JPanel();
		panel_7.add(panel_10, BorderLayout.CENTER);
		panel_10.setLayout(new BorderLayout(0, 0));
		
		JLabel lblPreview = new JLabel("Preview");
		lblPreview.setPreferredSize(new Dimension(38, 18));
		panel_10.add(lblPreview, BorderLayout.NORTH);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_10.add(scrollPane_1, BorderLayout.CENTER);
		
		table = new JTable();
		table.setRowHeight(17);
		scrollPane_1.setViewportView(table);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		panel_6.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		panel_2.add(btnCancel);
		
		JButton btnBack = new JButton("<< Back to filter");
		btnBack.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				filterCardLayout.show(filterCardPanel, "filter");
			}
		});
		panel_2.add(btnBack);
		
		JButton btnOk = new JButton("OK");
		panel_2.add(btnOk);
		btnOk.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				accepted=true;
				setVisible(false);
			}
		});
		
		
		if (actionType!=Action.Type.CHANGE_USER) {
			UppercaseDocumentFilter filter = new UppercaseDocumentFilter();
			((AbstractDocument) txFilter.getDocument()).setDocumentFilter(filter);
			((AbstractDocument) txRename.getDocument()).setDocumentFilter(filter);
			((AbstractDocument) txReplaceFrom.getDocument()).setDocumentFilter(filter);
			((AbstractDocument) txReplaceTo.getDocument()).setDocumentFilter(filter);
		}

		setLocationRelativeTo(null);
		
		action = new Action(actionType, getCurrentFilter(), getCurrentRename(), parentAction);
	}
	
	public boolean isAccepted() {
		return accepted;
	}
	
	public Action getAction() {
		return action;
	}
	

	//filter
	protected void changeFilterType() {
		updateFilterPreview();
	}	

	
	private void updateFilterPreview() {
		action.filter = getCurrentFilter();
		lsFilterPreview.setListData(action.getFilterResults(project,true));
	}
	
	private FilterData getCurrentFilter() {
		FilterData.Type type = FilterData.Type.EQUALS;  
		if (rbFilterStarts.isSelected()) 
			type = FilterData.Type.STARTS;
		else
		if (rbFilterEnds.isSelected()) 
			type = FilterData.Type.ENDS;
		else
		if (rbFilterContains.isSelected()) 
			type = FilterData.Type.CONTAINS;
		else
		if (rbFilterRegExpr.isSelected()) 
			type = FilterData.Type.REGEXP;
		
		return new FilterData(type,txFilter.getText());
	}

	//rename
	
	protected void changeRenameType() {
		if (rbNameNoChange.isSelected()) 
			replaceCardLayout.show(replaceCardPanel, "empty");
		else
		if (rbNameReplace.isSelected())
			replaceCardLayout.show(replaceCardPanel, "replace");
		else
			replaceCardLayout.show(replaceCardPanel, "new value");
		updateRenamePreview();
	}
	
	private RenameData getCurrentRename() {				
		RenameData.Type type = RenameData.Type.NOCHANGE;  
		if (rbNameSet.isSelected()) 
			type = RenameData.Type.SET;
		else
		if (rbNamePrefix.isSelected()) 
			type = RenameData.Type.PREFIX;
		else
		if (rbNameSuffix.isSelected()) 
			type = RenameData.Type.SUFFIX;
		else
		if (rbNameReplace.isSelected()) 
			type = RenameData.Type.REGEXP;		
		
		if (rbNameReplace.isSelected())
			return new RenameData(type,txReplaceFrom.getText(),txReplaceTo.getText());
		else
			return new RenameData(type,txRename.getText(),"");
	}

	private void updateRenamePreview() {
		action.rename = getCurrentRename();
		
		Vector<String> filterRes = action.getFilterResults(project); 
		String[][] modelData = new String[filterRes.size()][2];
		
		int row=0;
		for (String s : filterRes) {
			modelData[row][0]=s;
			modelData[row][1]=action.rename.getNewName(s,true);
			row++;
		}
	
		table.setModel(new DefaultTableModel(modelData,new String[] {"Original", "New name"}));
		
	}
	
	
	// inner classes 
	
	class RenameDocListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			updateRenamePreview();
		}
		@Override
		public void insertUpdate(DocumentEvent arg0) {
			updateRenamePreview();
		}
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			updateRenamePreview();
		}
	}
	
	class UppercaseDocumentFilter extends DocumentFilter {
	    public void insertString(DocumentFilter.FilterBypass fb, int offset, String text, AttributeSet attr) throws BadLocationException {
	        fb.insertString(offset, text.toUpperCase(), attr);
	    }

	    public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
	        fb.replace(offset, length, text.toUpperCase(), attrs);
	    }
	}
}
