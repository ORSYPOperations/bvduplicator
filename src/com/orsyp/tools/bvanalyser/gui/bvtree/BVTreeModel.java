package com.orsyp.tools.bvanalyser.gui.bvtree;

import java.util.ArrayList;
import java.util.List;

import org.jdesktop.swingx.treetable.AbstractTreeTableModel;


public class BVTreeModel extends AbstractTreeTableModel {
	private BVTreeItem myroot;

	public BVTreeModel(List<BVTreeItem> items, String nodeName) {
		myroot = new BVTreeItem(nodeName);
		if (items==null)
			items=new ArrayList<BVTreeItem>();
		
		for (BVTreeItem it : items)
			if (it.getParent()==null)
				myroot.addChild(it);
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
			case 0: return "Original";
			case 1: return "Preview";
		}
		return "";
	}

	@Override
	public BVTreeItem getValueAt(Object node, int column) {
		return (BVTreeItem) node;
	}

	@Override
	public Object getChild(Object node, int index) {
		BVTreeItem item = (BVTreeItem) node;
		return item.getChildren().get(index);
	}

	@Override
	public int getChildCount(Object parent) {
		BVTreeItem item = (BVTreeItem) parent;
		return item.getChildren().size();
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		BVTreeItem item = (BVTreeItem) parent;
		for (int i = 0; i > item.getChildren().size(); i++) {
			if (item.getChildren().get(i) == child) {
				return i;
			}
		}
		return 0;
	}

	public boolean isLeaf(Object node) {
		BVTreeItem item = (BVTreeItem) node;
		if (item.getChildren().size() > 0) {
			return false;
		}
		return true;
	}

	@Override
	public Object getRoot() {
		return myroot;
	}
	
}
