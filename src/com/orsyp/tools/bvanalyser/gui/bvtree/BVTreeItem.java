package com.orsyp.tools.bvanalyser.gui.bvtree;

import java.util.ArrayList;
import java.util.List;

public class BVTreeItem {
	public enum Type {NODE,BV,TASK};
	
	public Type type = Type.BV;
	
	public Object item;
	
	private BVTreeItem parent;
	private List<BVTreeItem> children = new ArrayList<BVTreeItem>();
	
	
	public BVTreeItem(Type type, BVTreeItem parent, Object item) {
		super();
		this.type = type;
		this.item = item;
		if (parent!=null)
			setParent(parent);
	}
	
	public BVTreeItem(String text) {
		super();
		this.type = Type.NODE;
		this.item = text;
	}

	/*
	private void setChildren(List<BVTreeItem> items) {
		children = items;
		for (BVTreeItem it: items)
			it.parent=this;
	}
	*/
	
	public void addChild(BVTreeItem item) {
		children.add(item);
		item.parent=this;
	}

	public List<BVTreeItem> getChildren() {
		return children;
	}

	public BVTreeItem getParent() {
		return parent;
	}

	public void setParent(BVTreeItem parent) {
		this.parent = parent;
		parent.children.add(this);
	}
	
}
