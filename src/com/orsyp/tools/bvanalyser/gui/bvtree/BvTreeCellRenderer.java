package com.orsyp.tools.bvanalyser.gui.bvtree;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.orsyp.tools.bvanalyser.gui.BVDuplicatorIcons;
import com.orsyp.tools.bvanalyser.gui.Styles;
import com.orsyp.tools.bvanalyser.model.BVObj;
import com.orsyp.tools.bvanalyser.model.TaskObj;

public class BvTreeCellRenderer extends DefaultTreeCellRenderer {
	
	private static final long serialVersionUID = 1L;	
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		BVTreeItem obj =null;
		if (value!=null) {
			obj = (BVTreeItem) value;
			String text ="";
			switch (obj.type) {
				case NODE:		text = (String)obj.item;
								break;
								
				case BV:		BVObj bvObj = (BVObj)(obj.item);
								text="<html>" + bvObj.getName();
								if (bvObj.getLabel()!=null)
									text += " - " +Styles.GRAYED + bvObj.getLabel() + Styles.GRAYED_END;
								text += "</html>";									
								break;
								
				case TASK:		TaskObj t = (TaskObj)(obj.item);
								String mu = "";
								String user = "";
								if (t.getTask().getMuName()!=null)
									if (t.getTask().getMuName().length()>0)
										mu = "&nbsp;&nbsp; <img src='file:img/mu_small.png'/> " + t.getTask().getMuName();
								if (t.getTask().getUserName()!=null)
									if (t.getTask().getUserName().length()>0)
										user = "&nbsp;&nbsp; <img src='file:img/user_small.png'/> " + t.getTask().getUserName();
								text="<html>" + t.getName() + Styles.GRAYED + mu + user + Styles.GRAYED_END + "</html>"; 
								break;
			}
			
	    	Component orig = super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);
	    	
	    	JLabel c = new JLabel();
			c.setOpaque(true);
			c.setBackground(orig.getBackground());
			c.setText(text);
			
			switch (obj.type) {
				case NODE:		c.setIcon(BVDuplicatorIcons.IMG_NODE);break;
				case BV:		c.setIcon(BVDuplicatorIcons.IMG_BV); break;
				case TASK:		c.setIcon(BVDuplicatorIcons.IMG_TASK); break;
			}
	    	
	    	return c;
		}
		else
			return super.getTreeCellRendererComponent(tree, null, sel, expanded, leaf, row, hasFocus);
    }

}
