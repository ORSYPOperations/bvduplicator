package com.orsyp.tools.bvanalyser.gui.bvtree;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.orsyp.tools.bvanalyser.BVDuplicator;
import com.orsyp.tools.bvanalyser.gui.Styles;
import com.orsyp.tools.bvanalyser.model.Action;
import com.orsyp.tools.bvanalyser.model.BVObj;
import com.orsyp.tools.bvanalyser.model.BVProject;
import com.orsyp.tools.bvanalyser.model.TaskObj;

public class BvTableCellRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	private BVDuplicator frame;
	
	public BvTableCellRenderer(BVDuplicator frame) {
		super();
		this.frame = frame;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
		BVProject project = frame.getProject();
		Component orig = super.getTableCellRendererComponent(table, "", isSelected, hasFocus, row, column);
		
		JLabel l = getLabel(orig);
		String tx = "";
		
		if (obj instanceof BVTreeItem) {
			BVTreeItem bvIt = (BVTreeItem) obj;
			switch (bvIt.type) {
				case NODE:	break; 
				case BV:	BVObj bvobj = (BVObj)bvIt.item;
							for (Action a: project.actions)
								if (a.type==Action.Type.RENAME_BV)
									if (a.filter.matches(bvobj.getName())) {
										if (tx.length()>0)
											tx+=", ";
										tx+= a.rename.getNewName(bvobj.getName());
									}
							if (tx.length()==0) {
								l.setFont(orig.getFont().deriveFont(Font.ITALIC));
								l.setForeground(Color.GRAY);
								tx="No action";
							}
							break;
				case TASK:	TaskObj tObj = (TaskObj)bvIt.item;
							//parent item
							BVTreeItem parentIt = bvIt.getParent();
							BVObj parentBvObj = (BVObj)parentIt.item;
							//get actions that match the parent bv
							Vector<Action> parentActions = new Vector<Action>(); 
							for (Action a: project.actions)
								if (a.type==Action.Type.RENAME_BV)
									if (a.filter.matches(parentBvObj.getName())) 
										parentActions.add(a);							
							
							for (Action parent: parentActions) {
								String block="";
								boolean taskChanged=false;
								//task renames
								int count=0;
								for (Action a: project.actions)
									if (a.parentAction==parent)
										if (a.type==Action.Type.RENAME_TASK)
											if (a.filter.matches(tObj.getName())) { 
												block+= a.rename.getNewName(tObj.getName())+ "&nbsp;&nbsp;" ;
												count++;
												taskChanged=true;
											}
								if (count==0)
									block+= tObj.getName()+ "&nbsp;&nbsp;" ;
								else
								if (count>1)
									block= Styles.ERROR + block+ Styles.ERROR_END;
									
								//mu changes
								count=0;
								String mublock="";
								for (Action a: project.actions)
									if (a.parentAction==parent)
										if (a.type==Action.Type.CHANGE_MU)
											if (a.filter.matches(tObj.getTask().getMuName())) {
												mublock+= "<img src='file:img/mu_small.png'/> " +Styles.GRAYED + a.rename.getNewName(tObj.getTask().getMuName()) + Styles.GRAYED_END + "&nbsp;&nbsp;" ;
												count++;
												taskChanged=true;
											}
								if (count>1)
									mublock= Styles.ERROR + mublock+ Styles.ERROR_END;
								block+=mublock;
								
								//user changes
								count=0;
								String userblock="";
								if (taskChanged)
									for (Action a: project.actions)
										if (a.parentAction==parent)
											if (a.type==Action.Type.CHANGE_USER)
												if (a.filter.matches(tObj.getTask().getUserName())) { 
													userblock+= "<img src='file:img/user_small.png'/> " +Styles.GRAYED +a.rename.getNewName(tObj.getTask().getUserName())+ Styles.GRAYED_END + "&nbsp;&nbsp;" ;
													count++;
												}
								if (count>1)
									userblock= Styles.ERROR + userblock+ Styles.ERROR_END;
								block+=userblock;
								
								if (block.length()>0 ) {
									if (tx.endsWith("&nbsp;&nbsp;"))
										tx = tx.substring(0,tx.length() - "&nbsp;&nbsp;".length()) + ",&nbsp;&nbsp;";  
									tx+=block;
								}
							}
							
							if (tx.length()==0) {
								l.setFont(orig.getFont().deriveFont(Font.ITALIC));
								l.setForeground(Color.GRAY);
								tx="No action";
							}
							else {
								tx = "<html>" + tx + "</html>";
							}
			}
		}
				
		l.setText(tx);
		return l;
	}
	
	private JLabel getLabel(Component orig) {
		JLabel l = new JLabel();
		l.setOpaque(true);
		l.setBackground(orig.getBackground());		
		return l;
	}

}
