package com.orsyp.tools.bvanalyser.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.Rectangle;

public class ErrorDlg extends JDialog {
	private static final long serialVersionUID = 1L;

	/**
	 * Create the dialog.
	 * @param errors 
	 */
	public ErrorDlg(Vector<String> errors) {
		setBounds(new Rectangle(0, 0, 400, 300));
		setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setBounds(100, 100, 589, 356);
		setTitle("Error log");
		
		getContentPane().setLayout(new BorderLayout());
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
	
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new EmptyBorder(4, 4, 0, 4));
		getContentPane().add(scrollPane, BorderLayout.CENTER);
	
		JList list = new JList();
		scrollPane.setViewportView(list);
		list.setListData(errors);
	
		setIconImage(new ImageIcon("img/du.png").getImage());	
		pack();
		setLocationRelativeTo(null);
					}

}
