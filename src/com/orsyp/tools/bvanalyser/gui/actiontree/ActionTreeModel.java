package com.orsyp.tools.bvanalyser.gui.actiontree;

import java.util.ArrayList;
import java.util.List;

import org.jdesktop.swingx.treetable.AbstractTreeTableModel;


public class ActionTreeModel extends AbstractTreeTableModel {
	private ActionTreeItem myroot;

	public ActionTreeModel(List<ActionTreeItem> items) {
		myroot = new ActionTreeItem();
		if (items==null)
			items=new ArrayList<ActionTreeItem>();
		
		for (ActionTreeItem it : items)
			if (it.getParent()==null)
				myroot.addChild(it);
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
			case 0: return "Action type";
			case 1: return "Source object filter";
			case 2: return "Target object action";
		}
		return "";
	}

	@Override
	public ActionTreeItem getValueAt(Object node, int column) {
		return (ActionTreeItem) node;
	}

	@Override
	public Object getChild(Object node, int index) {
		ActionTreeItem item = (ActionTreeItem) node;
		return item.getChildren().get(index);
	}

	@Override
	public int getChildCount(Object parent) {
		ActionTreeItem item = (ActionTreeItem) parent;
		return item.getChildren().size();
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		ActionTreeItem item = (ActionTreeItem) parent;
		for (int i = 0; i > item.getChildren().size(); i++) {
			if (item.getChildren().get(i) == child) {
				return i;
			}
		}
		return 0;
	}

	public boolean isLeaf(Object node) {
		ActionTreeItem item = (ActionTreeItem) node;
		if (item.getChildren().size() > 0) {
			return false;
		}
		return true;
	}

	@Override
	public Object getRoot() {
		return myroot;
	}
	
}
