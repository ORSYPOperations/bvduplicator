package com.orsyp.tools.bvanalyser.gui.actiontree;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.orsyp.tools.bvanalyser.gui.BVDuplicatorIcons;

public class ActionTreeCellRenderer extends DefaultTreeCellRenderer {
	
	private static final long serialVersionUID = 1L;	
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		ActionTreeItem obj =null;
		if (value!=null) {
			obj = (ActionTreeItem) value;
			String text ="";
			switch (obj.type) {
				case ROOT:		text="";break;
				case BV:		
				case TASK:		text=obj.action.getName(); break;
			}
			
	    	Component orig = super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);
	    	
	    	JLabel c = new JLabel();
			c.setOpaque(true);
			c.setBackground(orig.getBackground());
			c.setText(text);
			
			if (obj.action!=null)
				switch (obj.action.type) {
					case CHANGE_MU:		c.setIcon(BVDuplicatorIcons.IMG_MU);break;
					case CHANGE_USER:	c.setIcon(BVDuplicatorIcons.IMG_USER); break;
					case RENAME_BV:		c.setIcon(BVDuplicatorIcons.IMG_RENAME_BV); break;
					case RENAME_TASK:	c.setIcon(BVDuplicatorIcons.IMG_RENAME_TASK); break;
				}
			
	    	return c;
		}
		else
			return super.getTreeCellRendererComponent(tree, null, sel, expanded, leaf, row, hasFocus);
    }

}
