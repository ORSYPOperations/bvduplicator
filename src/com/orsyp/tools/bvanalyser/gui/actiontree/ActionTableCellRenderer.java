package com.orsyp.tools.bvanalyser.gui.actiontree;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class ActionTableCellRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
			
		Component orig = super.getTableCellRendererComponent(table, "", isSelected, hasFocus, row, column);
		
		String text ="";		
		
		if (obj instanceof ActionTreeItem) {
			ActionTreeItem ac = (ActionTreeItem) obj;
			if (!ac.type.equals(ActionTreeItem.Type.ROOT)) {
				switch (column) {
					case 1: text = ac.action.filter.getDescription(); break;
					case 2: text = ac.action.rename.getDescription(); break;
				}
			}
		}
		
		JLabel l = new JLabel();
		l.setText(text);
		l.setOpaque(true);
		//l.setFont(orig.getFont().deriveFont(Font.ITALIC));
		l.setBackground(orig.getBackground());
		//l.setForeground(Color.GRAY);
		
		return l;
	}

}
