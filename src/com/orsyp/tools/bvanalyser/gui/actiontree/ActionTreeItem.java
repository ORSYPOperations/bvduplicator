package com.orsyp.tools.bvanalyser.gui.actiontree;

import java.util.ArrayList;
import java.util.List;

import com.orsyp.tools.bvanalyser.model.Action;

public class ActionTreeItem {
	public enum Type {BV,TASK,ROOT};
	
	public Type type = Type.ROOT;
	
	public Action action;
	
	private ActionTreeItem parent;
	private List<ActionTreeItem> children = new ArrayList<ActionTreeItem>();
	
	
	public ActionTreeItem(Type type, ActionTreeItem parent, Action action) {
		super();
		this.type = type;
		this.action = action;
		if (parent!=null)
			setParent(parent);
	}
		
	public ActionTreeItem() {
	}

	public void addChild(ActionTreeItem item) {
		children.add(item);
		item.parent=this;
	}

	public List<ActionTreeItem> getChildren() {
		return children;
	}

	public ActionTreeItem getParent() {
		return parent;
	}

	public void setParent(ActionTreeItem parent) {
		this.parent = parent;
		parent.children.add(this);
	}
	
}
