package com.orsyp.tools.bvanalyser.gui;

import javax.swing.ImageIcon;

public class BVDuplicatorIcons {

	public static final ImageIcon IMG_NODE = new ImageIcon("img/server.png");
	public static final ImageIcon IMG_BV = new ImageIcon("img/bv.png");
	public static final ImageIcon IMG_TASK = new ImageIcon("img/task.png");
	
	public static final ImageIcon IMG_RENAME_BV = new ImageIcon("img/rename_bv.png");
	public static final ImageIcon IMG_RENAME_TASK = new ImageIcon("img/rename_task.png");
	public static final ImageIcon IMG_USER = new ImageIcon("img/user.png");
	public static final ImageIcon IMG_MU = new ImageIcon("img/mu.png");

}
