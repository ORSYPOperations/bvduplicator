package com.orsyp.tools.bvanalyser.gui;

public class Styles {

	//public final String STYLE = "<span style='color:#FF0000'>";
	//public final String STYLE_END = "</span>";
	public final static String HL = "<span style='background-color:#FFF060'><b>";
	public final static String HL_END = "</b></span>";
	
	public final static String GRAYED = "<span style='color:#808080'><i>";
	public final static String GRAYED_END = "</i></span>";
	
	public final static String ERROR = "<span style='background-color:#FF9999'><i>";
	public final static String ERROR_END = "</i></span>";
	
	public final static String ERRLOG = "<span style='color:#DD0000'><b>";
	public final static String ERRLOG_END = "</b></span>";
	
	public final static String WARNLOG = "<span style='color:#DD9900'><b>";
	public final static String WARNLOG_END = "</b></span>";	
}
