package com.orsyp.tools.bvanalyser.gui.util;

import java.io.File;

public class CustomFileFilter extends javax.swing.filechooser.FileFilter {
	
	public enum Type {DUP,ALL}
	
	private Type type = Type.ALL;
	
	@Override
	public boolean accept(File file) {
		if (file.isDirectory())
			return true;
        String filename = file.getName();
        switch (type) {
        	case DUP: return filename.toLowerCase().endsWith(".bvdup");
        }
        return true;
    }
	
    public String getDescription() {
    	switch (type) {
	    	case DUP: return "BV Duplicator project";
    	}
    	return "All files";
    }

	public CustomFileFilter(Type type) {
		super();
		this.type = type;
	}
    
}
