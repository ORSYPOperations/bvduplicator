package com.orsyp.tools.bvanalyser.gui;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.KeyStroke;

public class ConnectionDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	private JTextField txHost;
	private JTextField txPort;
	private JTextField txUser;
	private JPasswordField txPassword;
	private JTextField txNode;
	private JTextField txArea;
	
	private boolean accepted = false;
	

	/**
	 * Create the dialog.
	 */
	public ConnectionDialog(String node, String area, String host, String port, String user, String password) {
		setModal(true);
		setResizable(false);
		setTitle("UVMS Connection");
		setIconImage(new ImageIcon("img/duas.png").getImage());
		setBounds(100, 100, 310, 250);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);
		
		JButton btCancel = new JButton("Cancel");
		panel.add(btCancel);
		btCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		
		JButton btOk = new JButton("Ok");
		panel.add(btOk);
		btOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accepted=true;
				setVisible(false);
			}
		});
		
		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		
		JLabel label = new JLabel("UVMS server host");
		label.setBounds(10, 16, 113, 14);
		panel_1.add(label);
		
		txHost = new JTextField();
		txHost.setText(host);
		txHost.setBounds(122, 11, 170, 25);
		panel_1.add(txHost);
		
		JLabel label_1 = new JLabel("UVMS server port");
		label_1.setBounds(10, 44, 113, 14);
		panel_1.add(label_1);
		
		txPort = new JTextField();
		txPort.setText(port);
		txPort.setBounds(122, 39, 170, 25);
		panel_1.add(txPort);
		
		JLabel label_2 = new JLabel("User name");
		label_2.setBounds(10, 88, 113, 14);
		panel_1.add(label_2);
		
		txUser = new JTextField();
		txUser.setText(user);
		txUser.setBounds(122, 80, 170, 25);
		panel_1.add(txUser);
		
		JLabel label_3 = new JLabel("Password");
		label_3.setBounds(10, 113, 113, 14);
		panel_1.add(label_3);
		
		txPassword = new JPasswordField();
		txPassword.setText(password);
		txPassword.setBounds(122, 108, 170, 25);
		panel_1.add(txPassword);
		
				
		JLabel label_4 = new JLabel("Node");
		label_4.setBounds(10, 138, 113, 14);
		panel_1.add(label_4);
		
		txNode = new JTextField();
		txNode.setText(node);
		txNode.setBounds(122, 133, 170, 25);
		panel_1.add(txNode);
		
		JLabel label_5 = new JLabel("Area");
		label_5.setBounds(10, 163, 113, 14);
		panel_1.add(label_5);
		
		txArea = new JTextField();
		txArea.setText(area);
		txArea.setBounds(122, 158, 170, 25);
		panel_1.add(txArea);
		
		setLocationRelativeTo(null);
		
		getRootPane().registerKeyboardAction(new ActionListener() {
									        @Override
									        public void actionPerformed(ActionEvent e) {
												accepted = false;
												setVisible(false);
									        }
									    },
			    KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
			    JComponent.WHEN_IN_FOCUSED_WINDOW);
	}
		
	
	public boolean isAccepted() {
		return accepted;
	}

	public String getHost() {
		return txHost.getText();
	}

	public int getPort() {
		try {
			return Integer.parseInt(txPort.getText());
		} catch (Exception e) {
			return 0;
		}
	}

	public String getUser() {
		return txUser.getText();
	}

	public String getPassword() {
		return new String(txPassword.getPassword());
	}


	public String getNode() {
		return txNode.getText();
	}


	public String getArea() {
		return txArea.getText();
	}
}
